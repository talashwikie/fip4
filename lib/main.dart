
class MyObject<T,E>{
  T _value;
  E _value2;
  MyObject(this._value,this._value2);

  T get value => _value;

  set values(T value) {
     _value =  value;
  }
}
void callData(Function x){
  String myValue="toleen";
  print("call data");
  x.call(myValue);
}

Function hello (String value){
  print("this "+value);
}
class person {
  late String name;
  late int age ;
  late String data ;
}
_initData(person Person){
  Person.data="";
  print("ok");
}
void main() {
  MyObject <String, int>object = MyObject("hello", 5);
  print(object._value);
  print(object._value2);
  MyObject<bool, String>object2 = MyObject(false, "tala");
  print(object2._value);
  late List<int>data;
  //try {
  //  print(data.length);
  //}
  //catch (e) {
    //print(e.toString());
  //}
  //finally{
   // data=[];
    //print(data.length);
  //}
  print("hi");
  //callData(hallo());//null
  callData((){
    print("function passed");
  });
  callData(hello);
  List<int> ? ahmad;
  print(ahmad);
 // print(ahmad.length);//error
  print(ahmad ! .length);
  print(ahmad.indexOf(2)); // مرة وحدة
  person Person = person();
 // print(Person.age);//error// late لا تسمح بطباعة فراغ
 Person.age=55;
  print(Person.age);
  Person.name="";
  print(Person.name);
  print(Person.data??_initData(Person));
  print(Person.data);
}